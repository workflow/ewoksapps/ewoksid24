import os
import matplotlib.pyplot as plt

import numpy

from ewoksid24.io.temperature import read_temperature_data
from ewoksid24.plots.temperature import plot_temperature

from ewoksid24.fit.planck import fit_temperature_data

if __name__ == "__main__":
    filename = os.path.join(os.path.dirname(__file__), "LTC13_Fe_g1_LTC13_LH1.h5")
    scan_number = 39

    temp_up_data = read_temperature_data(filename, scan_number, "up", "T_US")
    temp_down_data = read_temperature_data(filename, scan_number, "down", "T_DS")

    def doplot():
        base_title = os.path.splitext(os.path.basename(filename))[0]
        for index in range(0, len(temp_up_data.epoch)):
            title = f"{base_title} #{scan_number} [{index}]"
            plot_temperature(temp_up_data, temp_down_data, index, title)

        T_up_avg = numpy.average(temp_up_data.planck_temperature)
        T_ds_avg = numpy.average(temp_down_data.planck_temperature)
        print(
            f"\nAverage temperatures: {temp_up_data.label} = {T_up_avg:.0f} K    {temp_down_data.label}> = {T_ds_avg:.0f} K"
        )

    doplot()

    fit_temperature_data(temp_up_data)
    fit_temperature_data(temp_down_data)

    doplot()

    plt.show()
