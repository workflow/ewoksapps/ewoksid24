# -*- coding: utf-8 -*-
"""
Created on Sun Oct 29 11:56:51 2023

@author: Boccato
"""

import os
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import h5py

# from scipy.optimize import curve_fit
mpl.rcParams["figure.dpi"] = 150


def Planck(wavelength, temperature, scale):
    h = 6.62607015e-34
    c = 2.998e8
    k = 1.380649e-23
    intensity = ((2 * c) / ((wavelength * 1e-9) ** 4)) * (
        1 / (np.exp((h * c) / ((wavelength * 1e-9) * k * temperature)) - 1)
    )
    return intensity * scale


# import h5 file
filename = os.path.join(os.path.dirname(__file__), "LTC13_Fe_g1_LTC13_LH1.h5")
runname = "39"
mini = 600
maxi = 950


f = h5py.File(filename, "r")

# for key in f.keys():
#     print(key) #Names of the root level object names in HDF5 file - can be groups or datasets.
#     print(type(f[key])) # get the object type: usually group or dataset

group = f[f"{runname}.2"]
# #Checkout what keys are inside that group.
# for key in group.keys():
#     print(key)

data = group["measurement"]

# for key in data.keys():
#     print(key)
# #dfy1= np.array(y1.value)

# for index in range(0,len(np.array(data['laser_heating_up_spectrum_lambdas']))):


for index in range(0, len(np.array(data["laser_heating_up_spectrum_lambdas"]))):
    x_up = np.array(data["laser_heating_up_spectrum_lambdas"])[index]
    x_ds = np.array(data["laser_heating_down_spectrum_lambdas"])[index]
    mask = np.logical_and(x_up >= 500, x_up <= 950)

    y_up_max = np.array(data["laser_heating_up_max_data"])[index]
    y_ds_max = np.array(data["laser_heating_down_max_data"])[index]

    y_us_planck = np.array(data["laser_heating_up_planck_data"])[index]
    y_ds_planck = np.array(data["laser_heating_down_planck_data"])[index]

    y_us_fit = np.array(data["laser_heating_up_planck_fit"])[index]
    y_ds_fit = np.array(data["laser_heating_down_planck_fit"])[index]

    fig, ax = plt.subplots(nrows=3, ncols=2, sharex=True)
    fig.suptitle(f"{filename[:-3]}_{runname}.2 [{index}]")

    T_up = np.array(data["laser_heating_up_T_planck"])[index]
    ax[0, 0].set_title(f"T_US = {T_up:.0f} K")

    T_ds = np.array(data["laser_heating_down_T_planck"])[index]
    ax[0, 1].set_title(f"T_DS = {T_ds:.0f} K")

    ax[0, 0].plot(x_up, y_up_max)
    ax[0, 1].plot(x_ds, y_ds_max)

    ax[1, 0].plot(x_up, y_us_planck)

    # p0 = [2760,1e-19]
    # #plt.plot(x_up,Planck(x_up,*p0))
    # ax[1, 0].plot(x_up,Planck(x_up,*p0))

    # popt, pcov = curve_fit(Planck, x_up, y_us_planck)#, bounds=(0, [3., 1., 0.5]))

    ax[1, 1].plot(x_ds, y_ds_planck)
    ax[1, 0].plot(x_up, y_us_fit)
    ax[1, 1].plot(x_ds, y_ds_fit)
    ax[1, 0].set_xlim(485, 950)
    ax[1, 0].set_ylim(0, max(y_us_planck[mask]))
    ax[1, 1].set_ylim(0, max(y_ds_planck[mask]))

    intensity_us_wien = np.log(y_us_planck * x_up**5)
    intensity_ds_wien = np.log(y_ds_planck * x_ds**5)
    T_up = np.array(data["laser_heating_up_T_planck"])[index]
    T_ds = np.array(data["laser_heating_down_T_planck"])[index]

    delta = 100
    Num_up = (
        np.log(y_us_planck[mask] / x_up[mask] ** 5)[:-delta]
        - np.log(y_us_planck[mask] / x_up[mask] ** 5)[delta:]
    )
    Den_up = 1 / (x_up[mask][:-delta] * 1e-9) - 1 / (x_up[mask][delta:] * 1e-9)
    Tcol_up = Num_up / Den_up

    Num_ds = (
        np.log(y_ds_planck[mask] / x_ds[mask] ** 5)[:-delta]
        - np.log(y_ds_planck[mask] / x_ds[mask] ** 5)[delta:]
    )
    Den_ds = 1 / (x_ds[mask][:-delta] * 1e-9) - 1 / (x_ds[mask][delta:] * 1e-9)
    Tcol_ds = Num_ds / Den_ds

    ax[2, 0].plot(x_up[mask][:-delta], Tcol_up)
    ax[2, 1].plot(x_ds[mask][:-delta], Tcol_ds)

    ax[2, 0].set(xlabel="wavelength (nm)")
    ax[2, 1].set(xlabel="wavelength (nm)")
    ax[1, 0].set_xlim(485, 950)


# %%

group_XAS = f[f"{runname}.1"]
# Checkout what keys are inside that group.
# for key in group.keys():
#     print(key)

data = group_XAS["measurement"]
# for key in data.keys():
#     print(key)

time = group_XAS["instrument"]
# for key in time.keys():
#     print(key)


epoch_mu = np.array(group_XAS["instrument"]["epoch"]["data"])
mu = np.array(group_XAS["measurement"]["mu_trans"])

epoch_T = np.array(group["measurement"]["epoch"])
T_up = np.array(group["measurement"]["laser_heating_up_T_planck"])
T_ds = np.array(group["measurement"]["laser_heating_down_T_planck"])


fig, ax1 = plt.subplots()
color = "k"
ax1.set_xlabel("epoch")
ax1.set_ylabel("mu", color=color)
ax1.plot(epoch_mu, mu, color=color)
ax1.tick_params(axis="y", labelcolor=color)

ax2 = ax1.twinx()
color = "tab:red"
ax2.set_ylabel(
    "Temperature (K)", color=color
)  # we already handled the x-label with ax1
if len(epoch_T) == len(T_up):
    ax2.plot(epoch_T, T_up, "-o", color="green")
    ax2.plot(epoch_T, T_ds, "-o", color="blue")
else:
    ax2.plot(epoch_T[:-1], T_up, "-o", color="green")
    ax2.plot(epoch_T[:-1], T_ds, "-o", color="blue")

ax2.tick_params(axis="y", labelcolor=color)

fig.tight_layout()  # otherwise the right y-label is slightly clipped
plt.show()


print(
    f"\nAverage temperatures: T_US = {np.average(T_up):.0f} K    T_DS = {np.average(T_ds):.0f} K"
)


f.close()
