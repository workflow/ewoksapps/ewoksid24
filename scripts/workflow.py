import os

from ewokscore import execute_graph


if __name__ == "__main__":
    filename = os.path.join(os.path.dirname(__file__), "LTC13_Fe_g1_LTC13_LH1.h5")
    scan_number = 39
    with_xas = False

    if with_xas:
        read_task = "ewoksid24.tasks.read.XasTemperatureRead"
        plot_task = "ewoksid24.tasks.plot.XasTemperaturePlot"
    else:
        read_task = "ewoksid24.tasks.read.ScanTemperatureRead"
        plot_task = "ewoksid24.tasks.plot.ScanTemperaturePlot"

    output_directory = os.path.join(os.path.sep, "tmp", "id24")

    workflow = {
        "graph": {"id": "test_temperature_task"},
        "nodes": [
            {
                "id": 0,
                "task_identifier": read_task,
                "task_type": "class",
                "default_inputs": [
                    {"name": "filename", "value": filename},
                    {"name": "scan_number", "value": 39},
                    {"name": "retry_timeout", "value": 0},
                ],
            },
            {
                "id": 1,
                "task_identifier": plot_task,
                "task_type": "class",
                "default_inputs": [
                    {"name": "output_directory", "value": output_directory},
                    {"name": "show", "value": True},
                ],
            },
        ],
        "links": [{"source": 0, "target": 1, "map_all_data": True}],
    }

    result = execute_graph(workflow)

    pattern = os.path.join(
        output_directory, "LTC13_Fe_g1_LTC13_LH1", f"scan{scan_number}", "*.png"
    )

    assert result["filenames"]
    print(f"\n   eog {pattern}")
