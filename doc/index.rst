ewoksid24
=========

*ewoksid24* provides data processing workflows for ID24.

*ewoksid24* has been developed by the `Software group <http://www.esrf.eu/Instrumentation/software>`_
and the ID24 staff of the `European Synchrotron <https://www.esrf.eu/>`_.

Documentation
-------------

.. toctree::
    :maxdepth: 2

    api
